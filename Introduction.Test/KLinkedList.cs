/*
 * Copyright (c) 2020 Mikhail Zolotukhin <zomial@protonmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */
using System;
using System.Linq;
using NUnit.Framework;

namespace Introduction.Test
{
    [TestFixture]
    public class KLinkedListTests
    {

        [Test]
        public void AddValue()
        {
            var intList = new KLinkedList<int>();
            const int valueToInsert = 10;
            intList.Add(valueToInsert);
            Assert.AreEqual(intList.Count, 1);
            Assert.IsTrue(intList.Contains(valueToInsert));
        }

        [Test]
        public void AddMultipleValues()
        {
            var intList = new KLinkedList<int>();
            int[] valuesToInsert = {1, 2, 5};
            
            foreach (var i in valuesToInsert)
            {
                intList.Add(i);
            }
            
            Assert.AreEqual(valuesToInsert.Length, intList.Count);
            foreach (var i in valuesToInsert)
            {
                Assert.IsTrue(intList.Contains(i));
            }
        }

        [Test]
        public void Clear()
        {
            var intList = new KLinkedList<int> {1, 4, 5};
            
            intList.Clear();
            
            Assert.AreEqual(0, intList.Count);
            Assert.IsFalse(intList.Contains(1));
            Assert.IsFalse(intList.Contains(4));
            Assert.IsFalse(intList.Contains(5));
        }

        [Test]
        public void Contains()
        {
            var intList = new KLinkedList<string>{"ora", "muda", "dora"};
            
            Assert.IsTrue(intList.Contains("ora"));
            Assert.IsTrue(intList.Contains("muda"));
            Assert.IsTrue(intList.Contains("dora"));
            
            Assert.IsFalse(intList.Contains(null));
            Assert.IsFalse(intList.Contains("ari"));
        }

        [Test]
        [TestCase(new[]{"ora", "muda"}, "ora", Description = "Remove first")]
        [TestCase(new[]{"ora", "muda"}, "muda", Description = "Remove last")]
        [TestCase(new[]{"ora", "dora", "muda"}, "dora", Description = "Remove in between")]
        public void RemoveValidValue(string[] listCreationArray, string elementToRemove)
        {
            var list = new KLinkedList<string>(listCreationArray);

            list.Remove(elementToRemove);
            
            Assert.AreEqual(listCreationArray.Length-1, list.Count);
            Assert.IsFalse(list.Contains(elementToRemove));
        }
        
        [Test]
        [TestCase(new[]{"ora", "muda"}, "ari", Description = "Remove nonexistent")]
        [TestCase(new[]{"ora", "muda"}, null, Description = "Remove null")]
        public void RemoveNonValidValue(string[] listCreationArray, string elementToRemove)
        {
            var list = new KLinkedList<string>(listCreationArray);

            var removalResult = list.Remove(elementToRemove);
            
            Assert.IsFalse(removalResult);
            Assert.AreEqual(listCreationArray.Length, list.Count);
        }

        [Test]
        public void CopyToEmpty()
        {
             var list = new KLinkedList<string>{"ora", "muda"};

             var emptyArray = new string[0];
             Assert.Throws<ArgumentOutOfRangeException>(() => list.CopyTo(emptyArray, 0));
        }

        [Test]
        public void CopyToNarrowSpace()
        {
             var list = new KLinkedList<string>{"ora", "muda", "ari", "dora"};
             
             var emptyArray = new string[5];
             Assert.Throws<ArgumentException>(() => list.CopyTo(emptyArray, 3));
        }

        [Test]
        public void CopyToNormally()
        {
             var list = new KLinkedList<string>{"ora", "muda", "ari", "dora"};
             
             var plainArray = new string[5];
             Assert.DoesNotThrow(() => list.CopyTo(plainArray, 0));

             foreach (var element in list)
             {
                 Assert.IsTrue(plainArray.Contains(element)); 
             }
        }

        [Test]
        public void SetValue()
        {
            var list = new KLinkedList<string>{"sticky fingers", "ripple", "crazy diamond"};

            list[1] = "hamon";
            
            Assert.AreEqual("hamon", list[1]);
        }

        [Test]
        public void SetGetValueOutOfRange()
        {
            var list = new KLinkedList<string>{"sticky fingers", "ripple", "crazy diamond"};

            Assert.Throws<ArgumentOutOfRangeException>(() =>
            {
                list[10] = "hamon";
            });
            Assert.Throws<ArgumentOutOfRangeException>(() =>
            {
                var unused = list[-12];
            });
        }

        
        [Test]
        [TestCase(new[]{1, 2, 3}, new[]{3, 2, 1})]
        [TestCase(new[]{0}, new[]{0})]
        [TestCase(new[]{2, 5, 7, 0}, new[]{0, 7, 5, 2})]
        [TestCase(new[]{1, 1, 1}, new[]{1, 1, 1})]
        [TestCase(new int[]{}, new int[]{})]
        public void Reverse(int[] listCreationArray, int[] invertedListCreationArray)
        {
            var list = new KLinkedList<int>(listCreationArray);
            var invertedList = new KLinkedList<int>(invertedListCreationArray);
            
            list.Reverse();
            Assert.AreEqual(invertedList, list);
        }
    }
}