/*
 * Copyright (c) 2020 Mikhail Zolotukhin <zomial@protonmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */
using NUnit.Framework;

namespace Introduction.Test
{
    [TestFixture]
    public class InsertionSort
    {
        [Test]
        [TestCase(new[]{1, 2, 3}, ExpectedResult = new[]{1, 2, 3}, Description = "Already Sorted")]
        [TestCase(new[]{3, 2, 1}, ExpectedResult = new[]{1, 2, 3}, Description = "Reverse")]
        [TestCase(new[]{1, 3, 2}, ExpectedResult = new[]{1, 2, 3}, Description = "Last Swap")]
        [TestCase(new[]{2, 1, 3}, ExpectedResult = new[]{1, 2, 3}, Description = "First Swap")]
        [TestCase(new[]{1}, ExpectedResult = new[]{1}, Description = "One Element")]
        [TestCase(new int[]{}, ExpectedResult = new int[]{}, Description = "Empty")]
        [TestCase(new[]{1, 1, 1}, ExpectedResult = new[]{1, 1, 1}, Description = "Ones")]
        [TestCase(new[]{2, 1, 3, 3, 1, 2}, ExpectedResult = new[]{1, 1, 2, 2, 3, 3}, Description = "With Repeats")]
        [TestCase(new[]{2, 5, 3, 9, 1, 10}, ExpectedResult = new[]{1, 2, 3, 5, 9, 10}, Description = "Random")]
        public int[] Sort(int[] inputArray)
        {
            return InsertionSort<int>.Sort(inputArray);
        }
    }
}