/*
 * Copyright (c) 2020 Mikhail Zolotukhin <zomial@protonmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */
using NUnit.Framework;

namespace Introduction.Test
{
    [TestFixture]
    public class KBinaryTreeTests
    {
        
        [Test]
        [TestCase(new []{1}, Description = "One")]
        [TestCase(new []{1, 2}, Description = "Two")]
        [TestCase(new []{1, 2, 3}, Description = "Three")]
        [TestCase(new []{1, 2, 3, 4, 5, 6, 7, 8, 9, 10}, Description = "Multiple On Greater Side")]
        [TestCase(new []{10, 9, 8, 7, 6, 5, 4, 3, 2, 1}, Description = "Multiple On Lesser Side")]
        [TestCase(new []{123, 51, 14, 762, 1, 821, 23, 62, 17}, Description = "Multiple On Both Sides")]
        public void Add(int[] treeCreationArray)
        {
            var tree = new KBinaryTree<int>(treeCreationArray);

            foreach (var elementToAdd in treeCreationArray)
            {
                Assert.IsTrue(tree.Contains(elementToAdd));
            }
        }

        [Test]
        [TestCase(new []{1, 3, 4, 12, 51, 61, 61, 51})]
        public void Clear(int[] treeCreationArray)
        {
            var tree = new KBinaryTree<int>(treeCreationArray);
            foreach (var i in treeCreationArray)
            {
                tree.Add(i);
            }
            
            tree.Clear();

            foreach (var i in treeCreationArray)
            {
                Assert.IsFalse(tree.Contains(i));
            }
        }

        /*         5
                4    12
              3    10  13     */
        [Test]
        [TestCase(new []{5, 12, 13, 10, 4, 3}, 10, Description = "Remove with no child")]
        [TestCase(new []{5, 12, 13, 10, 4, 3}, 13, Description = "Remove with no child")]
        [TestCase(new []{5, 12, 13, 10, 4, 3}, 4, Description = "Remove with left one child")]
        [TestCase(new []{5, 12, 13, 10, 4, 3}, 12, Description = "Remove with two children")]
        [TestCase(new []{5, 12, 13, 10, 4, 3}, 5, Description = "Remove root")]
        [TestCase(new []{5, 12, 13, 10}, 5, Description = "Remove root with one child")]
        [TestCase(new []{5, 12, 13, 4, 3}, 12, Description = "Remove node with one child")]
        [TestCase(new []{5}, 5, Description = "Remove root in lonely tree")]
        public void RemoveOne(int[] treeCreationArray, int elementToRemove)
        {
            var tree = new KBinaryTree<int>(treeCreationArray);
            
            var removeResult = tree.Remove(elementToRemove);
            
            Assert.IsTrue(removeResult);
            Assert.IsFalse(tree.Contains(elementToRemove));
            foreach (var treeElement in treeCreationArray)
            {
                if (treeElement != elementToRemove)
                {
                    Assert.IsTrue(tree.Contains(treeElement));
                }
            }
        }

        [Test]
        [TestCase(new []{5, 12, 13, 10, 4, 3}, null, Description = "Try to remove null")]
        [TestCase(new []{5, 12, 13, 10, 4, 3}, 1231, Description = "Try to remove nonexistent")]
        public void RemoveIncorrectValue(int[] treeCreationArray, int elementToRemove)
        {
            var tree = new KBinaryTree<int>(treeCreationArray);
            
            var removeResult = tree.Remove(elementToRemove);
            
            Assert.IsFalse(removeResult);
        }

        [Test]
        public void CopyTo()
        {
            var tree = new KBinaryTree<int>{1, 2};

            var arr = new int[] { };

            Assert.Throws<IAmTooLazyException>(() => tree.CopyTo(arr, 0));
        }
    }
}