/*
 * Copyright (c) 2020 Mikhail Zolotukhin <zomial@protonmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */
#nullable enable
using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;

namespace Introduction
{
    public class KBinaryTree<T> : ICollection<T> where T : IComparable
    {
        private class Node
        {
            public Node(Node? parent, Node? lesserChild, Node? greaterChild, T value)
            {
                Parent = parent;
                LesserChild = lesserChild;
                GreaterChild = greaterChild;
                Value = value;
            }

            internal Node? Parent { set; get; }
            internal Node? LesserChild { set; get; }
            internal Node? GreaterChild { set; get; }
            internal T Value { set; get; }
        }

        private Node? Root { set; get; }

        public KBinaryTree(params T[] args)
        {
            foreach (var arg in args)
            {
                Add(arg);
            }
        }
        
        public void Add(T value)
        {
            if (value == null)
            {
                return;    
            }

            if (Root == null)
            {
                Root = new Node(null, null, null, value);
                return;
            }

            var currentNode = Root;
            while (true)
            {
                if (value.CompareTo(currentNode.Value) > 0) // Greater
                {
                    if (currentNode.GreaterChild != null)
                    {
                        currentNode = currentNode.GreaterChild;
                    }
                    else
                    {
                        currentNode.GreaterChild = new Node(currentNode, null, null, value);
                    }
                }
                else if (value.CompareTo(currentNode.Value) < 0) // Lesser
                {
                    if (currentNode.LesserChild != null)
                    {
                        currentNode = currentNode.LesserChild;
                    }
                    else
                    {
                        currentNode.LesserChild = new Node(currentNode, null, null, value);
                    }
                }
                else // Already exists
                {
                    return;
                }
            }
        }

        public void Clear()
        {
            Root = null;
        }

        public bool Contains(T item)
        {
            if (item == null)
            {
                return false;
            }
            
            foreach (var element in this)
            {
                if (item.Equals(element))
                {
                    return true;
                }
            }

            return false;
        }

        public void CopyTo(T[] array, int arrayIndex)
        {
            throw new IAmTooLazyException();
        }

        public bool Remove(T item)
        {
            var possibleNodeForDeletion = FindByValue(item);
            return RemoveNode(possibleNodeForDeletion);
        }

        public int Count { get; }

        public bool IsReadOnly => false;

        public IEnumerator<T> GetEnumerator()
        {
            var nodesEnumerator = NodesEnumerator();
            while(nodesEnumerator.MoveNext())
            {
                if (nodesEnumerator.Current != null)
                {
                    yield return nodesEnumerator.Current.Value;
                }
            }
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        private Node? FindByValue(T value)
        {
            var nodesEnumerator = NodesEnumerator();
            while (nodesEnumerator.MoveNext())
            {
                if (nodesEnumerator.Current != null && value.Equals(nodesEnumerator.Current.Value))
                {
                    return nodesEnumerator.Current;
                }
            }

            return null;
        }

        private IEnumerator<Node> NodesEnumerator(Node root = null) 
        {
            // Ability to do custom root
            if (root == null)
            {
                root = Root;
            }
            
            var stack = new Stack<Node>();
            if (root != null)
            {
                stack.Push(root);
            }
 
            while (stack.Any())
            {
                var currentNode = stack.Pop();
                yield return currentNode;
                 
                if (currentNode.LesserChild != null)
                {
                    stack.Push(currentNode.LesserChild);
                }
                 
                if (currentNode.GreaterChild != null)
                {
                    stack.Push(currentNode.GreaterChild);
                }
            }           
        }

        private bool RemoveNode(Node? node)
        {
            if (node == null)
            {
                return false;
            }

            var hasNoChildren = node.LesserChild == null && node.GreaterChild == null;
            var hasTwoChildren = node.LesserChild != null && node.GreaterChild != null;
            
            if (hasNoChildren)
            {
                return RemoveNodeWithNoChildren(node);
            }
            else if (hasTwoChildren) 
            {
                return RemoveNodeWithTwoChildren(node);
            }
            else // hasOneChild
            {
                return RemoveNodeWithOneChild(node);
            }
        }

        private bool RemoveNodeWithTwoChildren(Node node)
        {
            var minimumNode = FindMinimumNodeInSubtreeWithRoot(node);
            node.Value = minimumNode.Value;
            return RemoveNode(minimumNode);
        }

        private bool RemoveNodeWithOneChild(Node node)
        {
            var child = node.LesserChild ?? node.GreaterChild;

            if (node.Parent == null) // is Root
            {
                Root = child;
                return true;
            }

            if (node == node.Parent.LesserChild)
            {
                node.Parent.LesserChild = child;
            }
            else // Greater child 
            {
                node.Parent.GreaterChild = child;
            }

            return true;
        }

        private bool RemoveNodeWithNoChildren(Node node)
        {
            if (node.Parent == null) // is Root
            {
                Root = null;
                return true;
            }

            if (node == node.Parent.LesserChild)
            {
                node.Parent.LesserChild = null;
            }
            else // GreaterChild
            {
                node.Parent.GreaterChild = null;
            }

            return true;
        }

        private Node? FindMinimumNodeInSubtreeWithRoot(Node? node)
        {
            var nodesEnumerator = NodesEnumerator(node);
            Node? currentMinimumNode = null;
            while (nodesEnumerator.MoveNext())
            {
                if (nodesEnumerator.Current != null
                    && (currentMinimumNode == null
                        || nodesEnumerator.Current.Value.CompareTo(currentMinimumNode.Value) < 0))
                {
                    currentMinimumNode = nodesEnumerator.Current;
                }
            }

            return currentMinimumNode;
        }
    }
}