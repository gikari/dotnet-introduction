/*
 * Copyright (c) 2020 Mikhail Zolotukhin <zomial@protonmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */
#nullable enable
using System;
using System.Collections;
using System.Collections.Generic;

namespace Introduction
{
    public class KLinkedList<T> : ICollection<T> where T : IEquatable<T>
    {
        private class Node
        {
            internal Node? Next { set; get; }
            internal Node? Prev { set; get; }
            internal T Value { set; get; }

            internal Node(T value, Node? next, Node? prev)
            {
                Value = value;
                Next = next;
                Prev = prev;
            }
        }

        public int Count
        {
            get
            {
                var count = 0;
                for (var currentNode = Head; currentNode != null; currentNode = currentNode.Next)
                {
                    count++;
                }
    
                return count;
            }
        }

        public bool IsReadOnly => false;

        public KLinkedList(params T[] args)
        {
            foreach (var arg in args)
            {
                Add(arg);
            }
        }

        public T this[int index]
        {
            get
            {
                if (index < 0 || index >= Count)
                {
                    throw new ArgumentOutOfRangeException(nameof(index));
                }

                var i = 0;
                for (var currentNode = Head; currentNode != null; currentNode = currentNode.Next)
                {
                    if (i == index)
                    {
                        return currentNode.Value;
                    }
                    
                    i++;
                }
                throw new InvalidOperationException();
            }
            set
            {
                if (index < 0 || index >= Count)
                {
                    throw new ArgumentOutOfRangeException(nameof(index));
                }

                var i = 0;
                for (var currentNode = Head; currentNode != null; currentNode = currentNode.Next)
                {
                    if (i == index)
                    {
                        currentNode.Value = value;
                        return;
                    }
                    
                    i++;
                }
            }
        }
        
        public IEnumerator<T> GetEnumerator()
        {
            for (var currentNode = Head; currentNode != null; currentNode = currentNode.Next)
            {
                yield return currentNode.Value;
            }
        }
        public void Add(T item)
        {
            var newNode = new Node(item, null, Tail);
            if (Tail != null)
            {
                Tail.Next = newNode;
                Tail = newNode;
            }
            else
            {
                Tail = newNode;
                Head = Tail;
            }
        }

        public void Clear()
        {
            Head = null;
            Tail = null;
        }

        public bool Contains(T item)
        {
            if (item == null)
            {
                return false;
            }
            
            for (var currentNode = Head; currentNode != null; currentNode = currentNode.Next)
            {
                if (item.Equals(currentNode.Value))
                {
                    return true;
                }
            }

            return false;
        }

        public void CopyTo(T[] array, int arrayIndex)
        {
            if (arrayIndex < 0 || arrayIndex >= array.Length)
            {
                throw new ArgumentOutOfRangeException(nameof(arrayIndex), "Array index is out of range!");
            }

            if (array.Length - arrayIndex < Count)
            {
                throw new ArgumentException("Array is too small for copying given list!");
            }
            
            var currentArrayIndex = arrayIndex;
            for (var currentNode = Head; currentNode != null; currentNode = currentNode.Next)
            {
                array[currentArrayIndex] = currentNode.Value;
                currentArrayIndex++;
            }
        }

        public bool Remove(T item)
        {
            if (item == null)
            {
                return false;
            }
            
            for (var currentNode = Head; currentNode != null; currentNode = currentNode.Next)
            {
                if (!item.Equals(currentNode.Value)) continue;
                RemoveNode(currentNode);
                return true;
            }

            return false;
        }

        public void Reverse()
        {
            var currentNode = Head;
            while (currentNode != null)
            {
                var nextNode = currentNode.Next;
                currentNode.Next = currentNode.Prev;
                currentNode.Prev = nextNode;

                currentNode = nextNode;
            }

            var tmp = Head;
            Head = Tail;
            Tail = tmp;
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        private Node? Head { set; get; }
        private Node? Tail { set; get; }

        private void RemoveNode(Node currentNode)
        {
            if (currentNode.Prev != null)
            {
                currentNode.Prev.Next = currentNode.Next;
            }
            else
            {
                Head = currentNode.Next;
            }

            if (currentNode.Next != null)
            {
                currentNode.Next.Prev = currentNode.Prev;
            }
            else
            {
                Tail = currentNode.Prev;
            }
        }
    }
}