/*
 * Copyright (c) 2020 Mikhail Zolotukhin <zomial@protonmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */
using System;

namespace Introduction
{
    public static class InsertionSort<T> where T : IComparable
    {
        public static T[] Sort(T[] arrayToSort)
        {
            var sortedArray = new T[arrayToSort.Length];
            arrayToSort.CopyTo(sortedArray, 0);
            for (var i = 1; i < sortedArray.Length; ++i)
            {
                var currentValue = sortedArray[i];
                
                int j;
                for (j = i - 1; j >= 0 && sortedArray[j].CompareTo(currentValue) > 0; j--)
                {
                    sortedArray[j + 1] = sortedArray[j];
                } 
                sortedArray[j + 1] = currentValue; 
            }

            return sortedArray;
        }
    }
}